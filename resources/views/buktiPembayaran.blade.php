@extends('main')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-7 col-lg-6 bg-white rounded p-2 px-3">
            <h5 class="fw-bold mb-1 text-center">Pembayaran Tagihan PBB {{ $resApi->detail->KotaKabupaten }}</h5>
            <h5 class="fw-bold mb-3 text-center">{{ $code }} Telah Berhasil</h5>
            <div class="w-100 border rounded pt-2 px-2 mb-2">
                <table class="table table-borderless">
                    <tr>
                        <th>NOP</th>
                        <td width="1">:</td>
                        <td>{{ $code }}</td>
                    </tr>
                    <tr>
                        <th>Jenis Pajak</th>
                        <td width="1">:</td>
                        <td>PBB</td>
                    </tr>
                    {{-- <tr>
                        <th>Nama</th>
                        <td width="1">:</td>
                        <td>Jarwo</td>
                    </tr> --}}
                    <tr>
                        <th>Alamat</th>
                        <td width="1">:</td>
                        <td>{{ $resApi->detail->Alamat }}</td>
                    </tr>
                    <tr>
                        <th>Kota/Kabupaten</th>
                        <td width="1">:</td>
                        <td>{{ $resApi->detail->KotaKabupaten }}</td>
                    </tr>
                    <tr>
                        <th>Status Bayar</th>
                        <td width="1">:</td>
                        <td>Lunas</td>
                    </tr>
                    <tr>
                        <th>NTB</th>
                        <td width="1">:</td>
                        <td>{{ $resApi->detail->NTB }}</td>
                    </tr>
                    <tr>
                        <th>NTPD</th>
                        <td width="1">:</td>
                        <td>{{ $resApi->detail->NTPD }}</td>
                    </tr>
                    <tr>
                        <th>Denda</th>
                        <td width="1">:</td>
                        <td>Rp {{ number_format($resApi->detail->denda, 0, '.', '.') }}</td>
                    </tr>
                    <tr>
                        <th>Total</th>
                        <td width="1">:</td>
                        <td>Rp {{ number_format($resApi->final_amount, 0, '.', '.') }}</td>
                    </tr>
                </table>
            </div>
            <p class="mb-0 fs-6">Terimakasih atas kepercayaan kamu bertransaksi di Billing Invoice Anilo.</p>
            <a href="{{ url('/') }}" class="btn btn-outline-secondary btn-sm w-100">Kembali</a>
        </div>
    </div>
@endsection
