@extends('main')
@section('content')
    <div class="row">
        <div class="col-md-2">
            <div class="bg-white p-3 rounded">
                <h6>Menu</h6>
                <hr class="mt-1 mb-2" />
                <ul class="w-100 px-0" style="list-style: none;">
                    <li class="mb-2">
                        <a class="nav-link text-decoration-underline text-primary" href="{{ url('/') }}">> PBB</a>
                    </li>
                    <li class="">
                        <a class="nav-link" href="javascript:;">> Restribusi Sampah</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-10">
            <div class="bg-white p-3 rounded">
                <h4 class="fw-normal mb-3">Yuk Bayar Pajak Kamu disini</h4>
                <hr />
                @if (session('status'))
                    <div class="alert alert-danger alert-dismissible fade show text-center text-uppercase fw-bold"
                        role="alert">
                        {{ session('status') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <form action="{{ url('/') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <div class="form-floating">
                                <input type="text" class="form-control" id="jenis_pajak" value="PBB"
                                    name="jenis_pajak" readonly>
                                <label for="jenis_pajak">Jenis Pajak</label>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <div class="form-floating">
                                <input type="text" class="form-control" id="propinsi" value="Jawa Tengah"
                                    name="provinsi" readonly>
                                <label for="propinsi">Propinsi</label>
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <div class="form-floating">
                                <select class="form-select" id="kota_kab" aria-label="Floating label select example"
                                    name="kota_kab" required>
                                    <option selected value="">Pilih Kota/Kabupaten</option>
                                    @foreach ($jateng['kota_kabupaten'] as $item)
                                        <option value="{{ $item['nama'] }}">{{ $item['nama'] }}</option>
                                    @endforeach
                                </select>
                                <label for="kota_kab">Kota/Kab</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating mb-3">
                                <input type="text" class="form-control" id="nop"
                                    placeholder="Nomor Objek Pajak/NOP" name="nop" required>
                                <label for="nop">Nomor Objek Pajak/NOP</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating mb-3">
                                <select class="form-select" id="sppt" aria-label="Floating label select example"
                                    name="tahun" required>
                                    <option selected value="">Pilih ---</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                </select>
                                <label for="sppt">Tahun Pajak SPPT</label>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end mb-3" id="displayInquiry">
                        <button type="submit" class="btn btn-primary" id="btnInquiry">Lanjut</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
