<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PbbController extends Controller
{
    public function index()
    {
        $data['jateng'] = Http::get('https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=33');
        return view('home', $data);
    }

    public function inquiry()
    {
        // return request();
        $reqApi = [
            'jenis'     => "5",
            "tujuan"    => request()->nop
        ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://167.172.69.139:18090/api/adapters/pbb',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($reqApi),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $resApi = json_decode($response);
        // return $resApi;

        if ($resApi->data->customer_data != 'taghihan tidak ditemukan') {
            $data['resApi'] = $resApi->data->customer_data;
            $data['data'] = request()->all();
            return view('konfirmasiBayar', $data);
        } else {
            return redirect('/');
        }
    }

    public function bukti_pembayaran($code)
    {
        $reqApi = [
            'jenis'     => "6",
            "tujuan"    => $code
        ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://167.172.69.139:18090/api/adapters/pbb',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($reqApi),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $resApi = json_decode($response);

        if ($resApi->data->customer_data != 'taghihan tidak ditemukan') {
            $data['resApi'] = $resApi->data->customer_data;
            $data['code'] = $code;

            return view('buktiPembayaran', $data);
        } else {
            return redirect('/');
        }
    }
}
