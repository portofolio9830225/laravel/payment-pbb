<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Billing Service - Anilo</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous"> --}}

    {{-- <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;1,500;1,700&display=swap"
        rel="stylesheet"> --}}

    <style>
        * {
            font-family: 'Roboto', sans-serif;
        }

        .header-img {
            /* background-position: center; */
            background-size: cover;
        }

        .bg-black {
            background-color: black;
            opacity: 0.4;
        }
    </style>
</head>

<body class="header-img" style="background-image: url({{ asset('img/login.jpg') }})">
    <div class="position-fixed w-100 bg-black" style="height: 100vh;"></div>
    <div class="container-fluid position-relative pt-4 pb-1" style="order: 99;">
        {{-- <div class="pt-4 pb-1"> --}}
        @yield('content')
        {{-- </div> --}}
    </div>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/jquery.js') }}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous">
    </script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js'
        integrity='sha512-k2WPPrSgRFI6cTaHHhJdc8kAXaRM4JBFEDo1pPGGlYiOyv4vnA0Pp0G5XMYYxgAPmtmv/IIaQA6n5fLAyJaFMA=='
        crossorigin='anonymous'></script> --}}
    @yield('js')
</body>

</html>
