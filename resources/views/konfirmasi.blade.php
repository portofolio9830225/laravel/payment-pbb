<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Billing Service - Anilo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;1,500;1,700&display=swap"
        rel="stylesheet">
    <style>
        * {
            font-family: 'Roboto', sans-serif;
        }

        .header-img {
            /* background-position: center; */
            background-size: cover;
        }

        .bg-black {
            background-color: black;
            opacity: 0.4;
        }
    </style>
</head>

<body class="header-img" style="background-image: url({{ asset('img/login.jpg') }})">
    <div class="position-fixed w-100 bg-black" style="height: 100vh;"></div>
    <div class="container position-relative" style="order: 99;">
        <div class="py-5">
            <div class="bg-white p-3 rounded">
                <h4 class="fw-normal mb-3">Yuk Bayar Pajak Kamu disini</h4>
                <hr />
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="form-floating">
                            <select class="form-select" id="floatingSelect" aria-label="Floating label select example"
                                disabled>
                                <option selected>PBB</option>
                            </select>
                            <label for="floatingSelect">{{ $input['jenis_pajak'] }}</label>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="form-floating">
                            <select class="form-select" id="floatingSelect" aria-label="Floating label select example"
                                disabled>
                                <option selected>{{ $input['provinsi'] }}</option>
                            </select>
                            <label for="floatingSelect">Propinsi</label>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="form-floating">
                            <select class="form-select" id="floatingSelect" aria-label="Floating label select example"
                                disabled>
                                <option selected>{{ $input['kota_kab'] }}</option>
                            </select>
                            <label for="floatingSelect">Kota/Kab</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" id="nop"
                                placeholder="Nomor Objek Pajak/NOP" value="{{ $input['nop'] }}" readonly>
                            <label for="nop">Nomor Objek Pajak/NOP</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <select class="form-select" id="sppt" aria-label="Floating label select example"
                                disabled>
                                <option value="{{ $input['tahun'] }}" selected>{{ $input['tahun'] }}</option>
                            </select>
                            <label for="sppt">Tahun Pajak SPPT</label>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <h5 class="card-header">Rincian Data</h5>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tr>
                                    <td style="width: 250px;">Nomor Objek Pajak/NOP</td>
                                    <td width="1">:</td>
                                    <td>31412321312312323</td>
                                </tr>
                                <tr>
                                    <td style="width: 250px;">Jenis Pajak</td>
                                    <td width="1">:</td>
                                    <td>PBB</td>
                                </tr>
                                <tr>
                                    <td style="width: 250px;">Daerah</td>
                                    <td width="1">:</td>
                                    <td>Jawa Barat</td>
                                </tr>
                                <tr>
                                    <td style="width: 250px;">Nama</td>
                                    <td width="1">:</td>
                                    <td>AGGREGATOR BGR 6</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between mt-3">
                    <div class="">
                        <a href="{{ url()->previous() }}" class="btn btn-outline-secondary">Kembali</a>
                    </div>
                    <div class="d-flex">
                        <div class="me-2">
                            <p style="font-size: 0.8rem;" class="text-secondary mb-0">Total Tagihan</p>
                            <p class="fw-bold mb-0 fs-6">Rp999.999</p>
                        </div>
                        <div class="">
                            <div class="btn btn-primary">Bayar</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous">
    </script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js'
        integrity='sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=='
        crossorigin='anonymous'></script>
</body>

</html>
