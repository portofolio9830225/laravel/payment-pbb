@extends('main')
@section('content')
    <div class="bg-white p-3 rounded">
        <h4 class="fw-normal mb-3">Yuk Bayar Pajak Kamu disini</h4>
        <hr />
        <div class="row">
            <div class="col-md-4 mb-3">
                <div class="form-floating">
                    <input type="text" class="form-control" id="jenis_pajak" value="PBB" name="jenis_pajak" readonly>
                    <label for="jenis_pajak">Jenis Pajak</label>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <div class="form-floating">
                    <input type="text" class="form-control" id="propinsi" value="Jawa Tengah" name="provinsi" readonly>
                    <label for="propinsi">Propinsi</label>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <div class="form-floating">
                    <input type="text" class="form-control" id="kota_kab" value="{{ $data['kota_kab'] }}"
                        name="provinsi" readonly>
                    <label for="kota_kab">Kota/Kab</label>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="form-floating">
                    <input type="text" class="form-control" id="nop" value="{{ $data['nop'] }}" name="provinsi"
                        readonly>
                    <label for="nop">Nomor Objek Pajak/NOP</label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-floating">
                    <input type="text" class="form-control" id="tahun" value="{{ $data['tahun'] }}" name="provinsi"
                        readonly>
                    <label for="tahun">Tahun Pajak SPPT</label>
                </div>
            </div>
        </div>
        <div class="card">
            <h5 class="card-header">Rincian Data</h5>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tr>
                                    <td style="width: 250px;">Luas Tanah</td>
                                    <td width="1">:</td>
                                    <td id="valLuasTanah">{{ $resApi->detail->Luastanah }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 250px;">Luas Bangunan</td>
                                    <td width="1">:</td>
                                    <td id="valLuasBangunan">{{ $resApi->detail->LuasBangunan }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 250px;">NTB</td>
                                    <td width="1">:</td>
                                    <td id="valNtb">{{ $resApi->detail->NTB }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 250px;">NTPD</td>
                                    <td width="1">:</td>
                                    <td id="valNtpd">{{ $resApi->detail->NTPD }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 250px;">Denda</td>
                                    <td width="1">:</td>
                                    <td id="valDenda">Rp {{ number_format($resApi->detail->denda, 0, '.', '.') }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 250px;">Total</td>
                                    <td width="1">:</td>
                                    <td id="valTotal">Rp {{ number_format($resApi->detail->total, 0, '.', '.') }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tr>
                                    <td style="width: 250px;">Nomor Objek Pajak/NOP</td>
                                    <td width="1">:</td>
                                    <td id="valNop">{{ $data['nop'] }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 250px;">Jenis Pajak</td>
                                    <td width="1">:</td>
                                    <td>PBB</td>
                                </tr>
                                <tr>
                                    <td style="width: 250px;">Alamat</td>
                                    <td width="1">:</td>
                                    <td id="valAlamat">{{ $resApi->detail->Alamat }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 250px;">Kota/Kabupaten</td>
                                    <td width="1">:</td>
                                    <td id="valKotaKab">{{ $resApi->detail->KotaKabupaten }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-between mt-3">
            <div class="">
                <a href="{{ url('/') }}" class="btn btn-outline-secondary">Kembali</a>
            </div>
            <div class="d-flex">
                <div class="me-2">
                    <p style="font-size: 0.8rem;" class="text-secondary mb-0 text-end">Total Tagihan</p>
                    <p class="fw-bold mb-0 fs-6 text-end" id="totalTagihan">Rp
                        {{ number_format($resApi->final_amount, 0, '.', '.') }}</p>
                </div>
                <div class="">
                    <a href="{{ url('bukti-pembayaran/' . $data['nop']) }}" class="btn btn-primary"
                        id="bayarTagihan">Bayar</a>
                </div>
            </div>
        </div>
    </div>
@endsection
