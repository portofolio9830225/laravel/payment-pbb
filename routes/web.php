<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PbbController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [PbbController::class, 'index']);
Route::post('/', [PbbController::class, 'inquiry']);
Route::get('/bukti-pembayaran/{code}', [PbbController::class, 'bukti_pembayaran']);
// Route::get('/', [PbbController::class, 'index']);
